# The STARRED pipeline

This folder contains all the scripts needed to generate a (narrow) PSF and to deconvolve a single image or a set of observations.

## 1. PSF Generation

You should first define a working directory that must contain the following paths:
* *(Required)* `data` with observed stars as `.npy` and/or `.fits`.
* *(Required)* `output`, in which the results will be saved.
* *(Optional)* `noise_map` with the noise map corresponding to each observation as `.npy` and/or `.fits`. Here you can include `.reg` files to mask contaminated regions (e.g. a close companion star). Given the i-th observation (with i starting in 0), the associated region file must be called `mask_i.reg`.

Then, to generate a narrow PSF:

    python 1_generate_psf.py --subsampling_factor FACTOR --niter N_ITER --lambda_scales L_SCALES --lambda_hf L_HF [--noise_map] -g GAIN -t T_EXP --data_path DATA_PATH [--noise_map_path NOISE_MAP_PATH] --output_path OUTPUT_PATH [--plot-interactive]

## 2. Deconvolution

You should first define a working directory that must contain the following subfolders:
* *(Required)* `data` with the observation(s) as `.npy`.
* *(Required)* `psf` with the corresponding psf(s) as `.npy`.
* *(Required)* `output`, in which the results will be saved.
* *(Optional)* `noise_map` with the noise map(s) corresponding the observation(s) as `.npy`.

Then, to deconvolve an image:

    python 2_deconvolve.py --subsampling_factor FACTOR --niter N_ITER --lambda_scales L_SCALES --lambda_hf L_HF [--noise_map] -g GAIN -t T_EXP --point_sources M --data_path DATA_PATH [--noise_map_path NOISE_MAP_PATH] --psf_path PSF_PATH --output_path OUTPUT_PATH