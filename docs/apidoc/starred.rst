starred package
===============

Subpackages
-----------

.. toctree::

    starred.deconvolution
    starred.plots
    starred.psf
    starred.utils


Module contents
---------------

.. automodule:: starred
    :members:
    :undoc-members:
    :show-inheritance: