Citing STARRED in a publication
===============================

If you want to acknowledge STARRED in a publication, we suggest you to cite:

* Michalewicz et al. 2023

  - STARRED: a two-channel deconvolution method with Starlet regularization, JOSS X Y ZZZZ
  - This paper briefly describes the functionality of the package
