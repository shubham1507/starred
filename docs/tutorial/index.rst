Tutorial
========

You can follow the example notebooks to learn how to use the core functionnality of STARRED:

- `Ground-based narrow PSF generation <https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/1_WFI%20narrow%20PSF%20generation.ipynb>`_

- `Ground-based joint deconvolution <https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/2_DESJ0602-4335%20joint%20deconvolution.ipynb>`_

- `Another ground-based joint deconvolution <https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/3_Another%20lensed%20quasar%20-%20joint%20deconvolution.ipynb>`_

- `James Webb Space Telescope (JWST) PSF generation and deconvolution <https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/4_JWST%20deconvolution.ipynb>`_


.. toctree::
	:maxdepth: 5
	:numbered:
	