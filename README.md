# STARRED: STARlet REgularized Deconvolution 

[![Python 3.8](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-380/)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![pipeline status](https://gitlab.com/cosmograil/starred/badges/main/pipeline.svg)](https://gitlab.com/cosmograil/starred/commits/main)
[![coverage report](https://gitlab.com/cosmograil/starred/badges/main/coverage.svg)](https://cosmograil.gitlab.io/starred/coverage/)


STARlet REgularized Deconvolution (STARRED) is a Python deconvolution method powered by Starlet regularization and JAX automatic differentiation. It uses a Point Spread Function (PSF) narrower than the original one as kernel. 

## Installation 
### Through Anaconda
We provide an Anaconda environment that satisfies all the dependencies in `starred-env.yml`. 
```bash
git clone https://gitlab.com/cosmograil/starred.git
cd starred
conda env create -f starred-env.yml
conda activate starred-env
pip install .
```
In case you have an NVIDIA GPU, this should automatically download the right version of JAX as well as cuDNN.
Next, you can run the tests to make sure your installation is working correctly.

```bash
# While still in the STARRED directory:
pytest . 
```

### Manually handling the dependencies
If you want to use an existing environment, just omit the Anaconda commands above:
```bash
git clone https://gitlab.com/cosmograil/starred
cd starred 
pip install .
```

or if you need to install it for your user only: 
```bash
python setup.py install --user 
```
    
## Requirements 

STARRED requires the following Python packages: 
* `astropy`
* `dill`
* `jax`
* `jaxlib`
* `matplotlib`
* `numpy`
* `scikit-image`
* `scipy`
* `optax`
* `tqdm`
    


## Example Notebooks and Documentation
The full documentation can be found [here](https://cosmograil.gitlab.io/starred/). 

Example notebooks are located in the [notebooks](https://gitlab.com/cosmograil/starred/-/tree/main/notebooks) folder: 
* [Ground-based narrow PSF generation](https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/1_WFI%20narrow%20PSF%20generation.ipynb)
* [Ground-based joint deconvolution](https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/2_DESJ0602-4335%20joint%20deconvolution.ipynb)
* [Another ground-based joint deconvolution](https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/3_Another%20lensed%20quasar%20-%20joint%20deconvolution.ipynb)
* [JWST PSF generation and deconvolution](https://gitlab.com/cosmograil/starred/-/blob/main/notebooks/4_JWST%20deconvolution.ipynb)

## Attribution

If you use this code, please cite [the paper](https://cosmograil.gitlab.io/starred/citing.html) indicated in the documentation.

## License
STARRED is a free software. You can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation.

STARRED is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY, without 
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details ([LICENSE.txt](LICENSE)).
