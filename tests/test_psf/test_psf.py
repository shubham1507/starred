from copy import deepcopy
import glob
import numpy as np
import os
import pytest
import unittest

from tests import TEST_PATH, OUT_PATH
from starred.utils.optimization import Optimizer
from starred.psf.psf import PSF
from starred.psf.loss import Loss
from starred.psf.parameters import ParametersPSF
from starred.utils import ds9reg
from starred.utils.noise_utils import propagate_noise
from numpy.testing import assert_allclose, assert_array_equal


class TestOptim(unittest.TestCase):
    def setUp(self):
        self.path = TEST_PATH
        self.outpath = OUT_PATH
        self.datapath = os.path.join(self.path, "data")
        self.noisepath = os.path.join(self.path, "noise_map")
        self.data = np.array([np.load(f) for f in sorted(glob.glob(os.path.join(self.datapath, '*200w_psfg.npy')))])

        self.norm = self.data[0].max() / 100.
        self.data /= self.norm
        self.sigma_2 = np.array([np.load(f) for f in sorted(glob.glob(os.path.join(self.noisepath, '*200w_psfg.npy')))]) ** 2
        self.sigma_2 /= self.norm ** 2
        self.subsampling_factor = 2

        self.N, self.image_size, _ = np.shape(self.data)

    def testoptim_psf(self):
        masks = np.ones((self.N, self.image_size, self.image_size))
        for i in range(self.N):
            possiblemaskfilepath = os.path.join(self.noisepath, 'mask_%s.reg'%str(i))
            if os.path.exists(possiblemaskfilepath):
                reg = ds9reg.Regions(self.image_size, self.image_size)
                reg.readds9(possiblemaskfilepath, verbose=True)
                reg.buildinvertedmask(verbose=True)
                masks[i,:,:] = reg.nomask

        model = PSF(image_size=self.image_size, number_of_sources=self.N, upsampling_factor=self.subsampling_factor)
        kwargs_init, kwargs_fixed, kwargs_up, kwargs_down = model.smart_guess(self.data, fixed_background=True, guess_method='barycenter')
        kwargs_init2, _, _, _ = model.smart_guess(self.data, fixed_background=True, guess_method='max')

        parameters = ParametersPSF(model, kwargs_init, kwargs_fixed, kwargs_up, kwargs_down)
        initial_values_kwargs = parameters.initial_values(as_kwargs=True)

        loss = Loss(self.data, model, parameters, self.sigma_2, N=self.N,
                    masks=masks,
                    regularization_terms='l1_starlet',
                    regularization_strength_scales=0, 
                    regularization_strength_hf=0)
        psf_optim = Optimizer(loss, parameters, method='trust-constr')
        psf_optim.minimize(maxiter=20,
                           restart_from_init=True, 
                           use_grad=True, 
                           use_hessian=False, 
                           use_hvp=True) 

        kwargs_partial = deepcopy(parameters.best_fit_values(as_kwargs=True))

        W = propagate_noise(model, np.sqrt(self.sigma_2), kwargs_partial, wavelet_type_list=['starlet'], method='SLIT',
                            num_samples=50,
                            seed=1, likelihood_type='chi2', verbose=False, upsampling_factor=2)[0]
        #releasing background
        kwargs_fixed = {
            'kwargs_moffat': {'fwhm': kwargs_partial['kwargs_moffat']['fwhm'],
                              'beta': kwargs_partial['kwargs_moffat']['beta'],
                              'C': kwargs_partial['kwargs_moffat']['C']},
            'kwargs_gaussian': {},
            'kwargs_background': {},
        }
        parameters = ParametersPSF(model, kwargs_partial, kwargs_fixed, kwargs_up, kwargs_down)
        loss = Loss(self.data, model, parameters, self.sigma_2, N=self.N, masks=masks, regularization_terms='l1_starlet',
            regularization_strength_scales=1, regularization_strength_hf=1,
            regularization_strength_positivity=0, W=W, regularize_full_psf=True)

        optim = Optimizer(loss, parameters, method='adabelief')
        best_fit, logL_best_fit, extra_fields, runtime = optim.minimize(
                    max_iterations=100, min_iterations=None,
                    init_learning_rate=1e-2, schedule_learning_rate=True,
                    restart_from_init=False, stop_at_loss_increase=False,
                    progress_bar=True, return_param_history=True
                    )

        loss_history = optim.loss_history
        param_history = optim.param_history
        kwargs_final = parameters.args2kwargs(best_fit)

        #test updates of the parameters
        for key1 in initial_values_kwargs.keys():
            for key2 in initial_values_kwargs[key1].keys():
                assert_array_equal(np.asarray(initial_values_kwargs[key1][key2]), np.asarray(kwargs_init[key1][key2]))

        parameters._update_arrays()
        parameters.update_kwargs(kwargs_final, kwargs_fixed, kwargs_up, kwargs_down)

        estimated_full_psf = model.model(0, **kwargs_final).reshape(32, 32)
        full_psf = model.get_full_psf(**kwargs_final, high_res=False)

        #testing export function
        model.export(self.outpath, kwargs_final, self.data, self.sigma_2, format='fits')
        model.export(self.outpath, kwargs_final, self.data, self.sigma_2, format='npy')
        with self.assertRaises(NotImplementedError):
            model.export(self.outpath, kwargs_final, self.data, self.sigma_2, format='unknown format')

        model.dump(os.path.join(self.outpath, 'model.pkl'), kwargs_final, self.norm)

        #testing the photometry and astrometry
        amp = np.asarray(model.get_amplitudes(**kwargs_final), dtype=np.float32)

        photom_high_res = np.asarray(model.get_photometry(**kwargs_final, high_res = True) * self.norm / self.subsampling_factor**2, dtype=np.float32)
        photom = np.asarray(model.get_photometry(**kwargs_final) * self.norm, dtype=np.float32)
        astrometry = np.asarray(model.get_astrometry(**kwargs_final), dtype=np.float32)
        astrometry_exp = np.asarray([[ 0.07377724,  0.1458687 ],
                                         [-0.1675866,   0.33976397],
                                         [ 0.32197064,  0.1409049 ]]
                                        , dtype=np.float32)

        print(astrometry)
        #check that this vector is constant :
        assert_allclose(amp/photom, np.asarray([(amp/photom)[0] for i in range(self.N)]), rtol=5e-2)
        assert_allclose(photom, photom_high_res, rtol=5e-2)
        assert_allclose(astrometry, astrometry_exp, atol=0.2)

        #test class updates
        loss.update_dataset(self.data, self.sigma_2, W, parameters)

        #test other configuration for the PSF model:
        model.include_moffat = False
        background = model.get_narrow_psf(**kwargs_final, norm=False)
        assert_allclose(background, kwargs_final['kwargs_background']['background'].reshape(self.image_size*self.subsampling_factor, self.image_size*self.subsampling_factor), atol=1e-8)

    def test_optax(self):
        model = PSF(image_size=self.image_size, number_of_sources=self.N, upsampling_factor=self.subsampling_factor)
        kwargs_init, kwargs_fixed, kwargs_up, kwargs_down = model.smart_guess(self.data, fixed_background=True)
        parameters = ParametersPSF(model, kwargs_init, kwargs_fixed, kwargs_up, kwargs_down)
        loss = Loss(self.data, model, parameters, self.sigma_2, N=self.N, masks=None, regularization_terms='l1_starlet',
            regularization_strength_scales=1, regularization_strength_hf=1,
            regularization_strength_positivity=0, W=None, regularize_full_psf=False)

        optim1 = Optimizer(loss, parameters, method='adam')
        optim2 = Optimizer(loss, parameters, method='adabelief')
        optim3 = Optimizer(loss, parameters, method='radam')

        optim1.minimize(parameters, max_iterations=10, min_iterations = 1, schedule_learning_rate=False,
                        stop_at_loss_increase=True, progress_bar=False,
                        return_param_history=True)
        optim1.minimize(parameters, max_iterations=10, min_iterations = 1, schedule_learning_rate=True,
                        stop_at_loss_increase=True, progress_bar=False,
                        return_param_history=True)
        optim2.minimize(parameters, max_iterations=10, min_iterations = 1, schedule_learning_rate=False)
        optim3.minimize(parameters, max_iterations=10, min_iterations = 1, schedule_learning_rate=False)
        optim3.minimize(parameters, max_iterations=10, min_iterations = 1, schedule_learning_rate=True)

    def test_scipyminimize(self):
        model = PSF(image_size=self.image_size, number_of_sources=self.N, upsampling_factor=self.subsampling_factor)
        kwargs_init, kwargs_fixed, kwargs_up, kwargs_down = model.smart_guess(self.data, fixed_background=True)
        parameters = ParametersPSF(model, kwargs_init, kwargs_fixed, kwargs_up, kwargs_down)
        loss = Loss(self.data, model, parameters, self.sigma_2, N=self.N, masks=None, regularization_terms='l1_starlet',
            regularization_strength_scales=1, regularization_strength_hf=1,
            regularization_strength_positivity=0, W=None, regularize_full_psf=False)

        optim1 = Optimizer(loss, parameters, method='BFGS')
        optim1.minimize(maxiter=5, restart_from_init=True, use_grad=True, use_hessian=False, use_hvp=False)

        optim2 = Optimizer(loss, parameters, method='trust-constr')
        optim2.minimize(maxiter=2,restart_from_init=True, use_grad=True, use_hessian=True, use_hvp=False)


    def test_raise(self):
        model = PSF(image_size=self.image_size, number_of_sources=self.N, upsampling_factor=self.subsampling_factor)
        kwargs_init, kwargs_fixed, kwargs_up, kwargs_down = model.smart_guess(self.data, fixed_background=True)
        with self.assertRaises(ValueError):
            _ = model.smart_guess(self.data, fixed_background=True, guess_method='unknown')
        parameters = ParametersPSF(model, kwargs_init, kwargs_fixed, kwargs_up, kwargs_down)
        masks = np.ones((self.N, self.image_size, self.image_size))

        loss = Loss(self.data, model, parameters, self.sigma_2, N=self.N,
                    masks=masks,
                    regularization_terms='l1_starlet',
                    regularization_strength_scales=0,
                    regularization_strength_hf=0)

        with self.assertRaises(NotImplementedError):
            Optimizer(loss, parameters, method='unknown')

        with self.assertRaises(NotImplementedError):
            optim = Optimizer(loss, parameters)
            optim.method = 'unknown'
            optim.minimize()

        with self.assertRaises(NotImplementedError):
            optim = Optimizer(loss, parameters)
            optim.method = 'unknown'
            optim._run_optax(parameters, schedule_learning_rate=True)

        with self.assertRaises(NotImplementedError):
            optim = Optimizer(loss, parameters)
            optim.method = 'unknown'
            optim._run_optax(parameters, schedule_learning_rate=False)

        with self.assertRaises(ValueError):
            optim = Optimizer(loss, parameters)
            optim.loss_history

        with self.assertRaises(ValueError):
            optim = Optimizer(loss, parameters)
            optim.param_history

        with self.assertRaises(KeyError):
            parameters.get_param_names_for_model('unknown')

        reg = ds9reg.Regions(self.image_size, self.image_size)
        with self.assertRaises(FileNotFoundError):
            reg.readds9('unknown_path', verbose=False)

if __name__ == '__main__':
    pytest.main()
