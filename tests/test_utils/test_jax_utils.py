import numpy as np
import jax.numpy as jnp
import scipy
import glob
from numpy.testing import assert_allclose
import os
import copy
import pytest
import unittest
import matplotlib.pyplot as plt

from starred.utils.jax_utils import WaveletTransform

from tests import TEST_PATH

class TestUtils(unittest.TestCase):

    def setUp(self):
        self.path = TEST_PATH
        self.datapath = os.path.join(self.path, "data")
        self.data = np.array([np.load(f) for f in sorted(glob.glob(os.path.join(self.datapath, '*200w_psfg.npy')))])
        self.plot = True

    def test_wavelets(self):
        starlet = WaveletTransform(7, wavelet_type='starlet')
        image = self.data[0]
        coeffs = starlet.decompose(image)
        rec_image = starlet.reconstruct(coeffs)

        assert_allclose(image, rec_image, atol = 1e-5)

        if self.plot:
            fig, ax = plt.subplots(1, 3, figsize=(10, 5))
            im1 = ax[0].imshow(np.log10(rec_image))
            im2 = ax[1].imshow(np.log10(image))
            im3 = ax[2].imshow(rec_image - image)
            ax[0].set_title('Image')
            ax[1].set_title('Image reconstructed')
            ax[2].set_title('Residuals')
            plt.show()

        #if n_scale = 0, the decomposition is the image
        starlet = WaveletTransform(0, wavelet_type='starlet')
        coeffs = starlet.decompose(image)
        assert_allclose(image, coeffs, atol=1e-8)

    def test_raise(self):
        with self.assertRaises(NotImplementedError):
            starlet = WaveletTransform(7, wavelet_type='starlet-gen2')

        with self.assertRaises(ValueError):
            starlet = WaveletTransform(7, wavelet_type='unknown')