import numpy as np
import glob
from numpy.testing import assert_allclose
import os
import pytest
import unittest
import matplotlib.pyplot as plt

from starred.utils.noise_utils import propagate_noise, dirac_impulse
from starred.psf.psf import PSF

from tests import TEST_PATH

class TestUtils(unittest.TestCase):

    def setUp(self):
        self.path = TEST_PATH
        self.datapath = os.path.join(self.path, "data")
        self.noisepath = os.path.join(self.path, "noise_map")
        self.data = np.array([np.load(f) for f in sorted(glob.glob(os.path.join(self.datapath, 'star**.npy')))])
        self.N, self.image_size, _ = np.shape(self.data)

        self.norm = self.data[0].max() / 100.
        self.sigma_2 = np.zeros((self.N, self.image_size, self.image_size))
        sigma_sky_2 = np.array([np.std(self.data[i,int(0.9*self.image_size):,int(0.9*self.image_size):]) for i in range(self.N)]) ** 2
        for i in range(self.N):
            self.sigma_2[i,:,:] = sigma_sky_2[i] + self.data[i,:,:].clip(min=0)

        self.sigma_2 /= self.norm ** 2
        self.data /= self.norm
        self.subsampling_factor = 2

        self.kwargs_partial = {
            'kwargs_moffat': {'fwhm': np.array([3.54344314]), 'beta': np.array([2.10782421]), 'C': np.array([115.30484381])},
            'kwargs_gaussian': {'a': np.array([-0.48029483, -0.55883527, -0.33968393]), 'x0': np.array([-0.26200011,  0.52001915,  0.31850829]), 'y0': np.array([-0.03379763, -0.12634879,  0.06106638])},
            'kwargs_background': {'background': np.array([0. for i in range((self.image_size*self.subsampling_factor)**2)])}
        }

        self.model =PSF(image_size=self.image_size, number_of_sources=self.N,
                    upsampling_factor=self.subsampling_factor,
                    convolution_method='fft',
                    include_moffat=True)

        self.plot = False

    def test_noise(self):
        W = propagate_noise(self.model, np.sqrt(self.sigma_2), self.kwargs_partial, wavelet_type_list=['starlet'], method='MC',
                            num_samples=500, seed=1, likelihood_type='chi2', verbose=False, upsampling_factor=self.subsampling_factor)[0]

        mean = np.mean(W, axis=(1,2))
        print(mean)
        mean_desired = [1.0713826323207312, 0.8427535655706919, 0.4846057191451246, 0.24844168471194444, 0.12184494114364737, \
               0.05723840416801891, 0.024992071982694243, 0.010988484928729203]

        if self.plot :
            gix, axs = plt.subplots(1, len(W), figsize=(12, 4))
            for i, l in enumerate(W):
                axs[i].imshow(l)
                print(np.mean(l))
            plt.show()

        assert_allclose(mean_desired, mean, atol=5e-2)

        dirac = dirac_impulse(self.image_size)
        assert np.sum(dirac) == 1

    def test_raise(self):
        with self.assertRaises(ValueError):
            W = propagate_noise(self.model, np.sqrt(self.sigma_2), self.kwargs_partial, likelihood_type='unknown')[0]

        with self.assertRaises(ValueError):
            W = propagate_noise(self.model, np.sqrt(self.sigma_2), self.kwargs_partial, method='unknown')[0]

        with self.assertRaises(TypeError):
            W = propagate_noise(np.array([1.]), np.sqrt(self.sigma_2), self.kwargs_partial)[0]

        with self.assertRaises(ValueError):
            W = propagate_noise(self.model, np.sqrt(self.sigma_2), self.kwargs_partial, wavelet_type_list=['battle-lemarie-1'])[0]
